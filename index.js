/*
	Mini Activity: 
		Create an expressjs API designated to port 4000.
		Create a new route with endpoint /hello and method GET.
			-Should be able to respond with "Hello World."
*/

const express = require("express");
const mongoose = require("mongoose");
//Mongoose is a package that allows creation of Schemas to model our data structure.
//Also has access to a number of methods for manipulating our database.

const app = express();

const port = 4000;

mongoose.connect("mongodb+srv://Admin:admin123@course-booking.cevr7.mongodb.net/B157_to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
});


//Set notifications for connection success or failure
//Connection to the database
//Allows us to handle errors when the initial connection is established
let db = mongoose.connection;
//if a connection error occured, output in the console.
db.on("error", console.error.bind(console, "Connection error"));

//If the connection is successful, output in the console.
db.once("open", () => console.log("We're connected to the cloud database"));


//Schemas determine the structure of the documents to be written in the database.
//This acts as a blueprints to our data
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}

});

//Model uses schemas and are used to create/instantiate objects that correspond to the schema
//Models must be in a singular form and capitalized.
//The first parameter of the mongoose model method indicates the collection in where will be stored in the MongoDB collection
//The second parameter is used to specify the schema/blueprint of the documents
//Using mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into plural form when creating a collection.
const Task = mongoose.model("Task", taskSchema);


//Setup for allowing the server to handle data from requests
//Allows our app to read json data
app.use(express.json());
//Allows our app to read data from forms
app.use(express.urlencoded({extended:true}));


//Creating a new Task
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task tasks doesnt exist, we add it in our database.
	2. The task data will be coming from the request's body
	3. Create a Task object with a "name" field/property

*/

app.post("/tasks", (req, res) => {

	//Check if there are duplicate tasks
	//If there are no matches, the value of result is null
	Task.findOne({name: req.body.name}, (err, result) => {

		//if a document was found and the document's name matches the information sent via the client/Postman
		if (result != null && result.name == req.body.name){
			//Return a message to the client/postman
			return res.send("Duplicate task found");

		//if no document was found	
		} else {

			//Create a new Task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});

			//save method will store the information to the database
			newTask.save((saveErr, savedTask) => {

				//if there are errors in saving
				if(saveErr){
					return console.error(saveErr)

				//no error found while creating the document	
				} else {

					//returns a status code of 201 and sends a message "New Task created."
					return res.status(201).send("New Task created.")
				}

			})

		}

	})

})


//GET request to retrieve all the documents.

/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are founf, send a success back to the client

*/

app.get("/task", (req, res)=>{

	Task.find({}, (err, result)=>{

		if (err) {
			console.log(err)
		}else{
			return res.status(200).json({
				dat:result
			})
		}
	})
})




app.get("/hello", (req, res) => {
	res.send("Hello World");
});


/*ACTIVITY*/
const userSchema = new mongoose.Schema({
	username: String,
	password: String

});

const User = mongoose.model("User", userSchema);


app.post("/signup", (req, res)=>{
	User.findOne({username: req.body.username},(err, result) => {

		if (result!= null && result.username == req.body.username){
				return res.send("User already exists. Input another.");
		} else {
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, savedUser)=>{
				if (saveErr){
					return console.error(saveErr)
		} else {
			return res.status(201).send("New user registered.")
		}
	})
		}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`));
